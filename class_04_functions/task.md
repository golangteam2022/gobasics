# Instructions For Task

- Create a script, which can take an input argument, and print its square.

## Hints:

- use the package `os` to read the input arguments.
    Input arguments are always strings. 
    - `os.Args[0]` is the program name
    - `os.Args[1]` is the first argument passed when the script is called.

- use the package `strconv` to parse float from string.
    - the function `strconv.ParseFloat(text, 64)` will parse a text into a 64bit float.

- you can use `fmt.Printf` to print a nice text with formatted floats in it.