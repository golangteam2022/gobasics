# Creating a new project

## Steps

- Step 1: Create an additional function outside of the `main` function in **main.go**
    ```go
    func add(x int, y int) int {
        return x + y
    }
    ```
- Step 2: Call the function and return its result from within the `main` function:
    ```go
	result := add(42, 13)
	fmt.Println(result)
    ```
- Step 3: run it!

- Step 4: let's try multiple return values with
    ```go
    func add_sub(x int, y int) (int, int) {
        return x + y, x - y
    }
    ```
    and calling it with
    ```go
	result1, result2 := add_sub(42, 13)
	fmt.Println(result1, result2)
    ```
- Step 5: Run it!