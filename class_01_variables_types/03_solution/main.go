package main

import (
	"fmt"
)

// declare variables with or without initial values
var (
	name    string
	age     int
	isVegan bool
)

func main() {
	// variables which have no values assigned, are initialized with default values, "" and 0
	fmt.Println(name, age, isVegan)
}
