package main

import (
	"fmt"
	"sync"
)

func makeSquares(array [10]int) {
	for index, elem := range array {
		array[index] = elem * elem
	}
}

func changeArray() {
	a := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	makeSquares(a)
	fmt.Println(a)
}

func printEmptyPointer() {
	var s *int
	fmt.Println(s)
}

func clearSlice() {
	// a := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	// a = nil
	// fmt.Println(a)
}

const N = 10

func waitAndMutex() {
	// Deadlock
	m := make(map[int]int)
	wg := &sync.WaitGroup{}
	mu := &sync.Mutex{}
	wg.Add(N)

	for i := 0; i < N; i++ {
		go func() {
			defer wg.Done()
			mu.Lock()
			m[i] = i
			mu.Lock()
		}()
	}

	wg.Wait()
	println(len(m))
}

func slicer() {
	a := [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	s := a[2:4]
	s[0] = 33
	fmt.Println(a[2])
	fmt.Println(cap(s)) // returns 7, why?
}

func runWithSelect() {
	go func() {
		fmt.Println("GoRoutine Running")
	}()
	select {}
	fmt.Println("Finished")
}

func main() {
	runWithSelect()
	// slicer()
	// waitAndMutex()
	// changeArray()
	// printEmptyPointer()
}
