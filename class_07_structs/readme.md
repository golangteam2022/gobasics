# Structs

We create a new struct, add some fields and initialize it.
We also take a look at inheritance in Golang

## Steps

- Step 1: We take a first look at how a struct is created. We define a struct on top of the code:
    ```go
    type Human struct {
        FirstName string
        LastName  string
        birthYear int
    }
    ```

- Step 1: and finally create an instance and print it using `"%#v"`
    ```go
    package main

    import "fmt"

    type Human struct {
        FirstName string
        LastName  string
        birthYear int
    }

    func main() {
        person := Human{"Billy", "Joel", 1949}
        fmt.Printf("%#v\n", person)
    }
    ```

- Step 2: Now change a field of the struct and print its new content:
    ```go
	person.FirstName = "William Martin"
	fmt.Printf("%#v\n", person)
	fmt.Printf("First Name: %#v\n", person.FirstName)
    ```

- Step 3:  Create a struct `Artist` which inherits from `Human`
    ```go
    type Artist struct {
        Human
        Genre string
        Songs int
    }
    ```
    and now we make a new instance
    ```go
	billy := Artist{person, "Rock & Pop", 10}
	fmt.Printf("%#v\n", billy)
    ```

- Step 4: finally, add a method, in Golang-style
    ```go
    func (a Artist) newReleaseA() int {
        a.Songs++
        return a.Songs
    }

    func (a *Artist) newReleaseB() int {
        a.Songs++
        return a.Songs
    }
    ```
- Step 5: We now call the method, and print the final instance after the changes:
    ```go
	me := &billy
	fmt.Printf("%s released their %dth song\n", me.FirstName, me.newReleaseB())
	fmt.Printf("%s has a total of %d songs", me.FirstName, me.Songs)
    ```