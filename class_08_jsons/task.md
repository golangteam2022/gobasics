# Instructions For Task

- make a program, which has a class `language` with the fields `Id`(int) and `Name` (string)

- it should be possible to create an array with 2 elements reading from this input:
    ```json
    "[{\"Id\": 100, \"Name\": \"Go\"}, {\"Id\": 200, \"Name\": \"Java\"}]"
    ```

- there should also be a method to export an array of language instance to a byte array with the same content.

- print the created string

