# JSONS

We create a new struct, add some fields and initialize it.
We also take a look at inheritance in Golang

## Steps

- Step 1: We start out again with a struct, but now add additional info how conversion to json happens:
    ```go
    type Human struct {
        FirstName string `json: "firstName"`
        LastName  string `json: "lastName"`
        birthYear int    `json: "birthYear"`
    }
    ```
    Additionally, we add a method which will transform
    ```go
    func toBytes(humans []Human) []byte {
    	jsonBytes, _ := json.Marshal(humans)
        return jsonBytes
    }
    ```
    The json package gives functions to convert object to or from json byte arrays.

- Step 2: Now we create an object and print its byte and string representation:
    ```go
	persons := []Human{{"Billy", "Joel", 1949}}
	text := toBytes(persons)
	fmt.Println(text)
	fmt.Println(string(text))
    ```

- Step 3: As next step, we look at reading a json and creating an object. First a Method for this is defined:
    ```go
    func fromBytes(text []byte) []Human {
        var humans []Human
        json.Unmarshal(text, &humans)
        return humans
    }
    ```
- Step 4: And finally, we pass a byte array to this method and create a clone object:
    ```go
    func fromBytes(text []byte) []Human {
        var humans []Human
        json.Unmarshal(text, &humans)
        return humans
    }
    ```
    and call it in the `main` function:
    ```go
	clonebytes := []byte("[{\"FirstName\":\"Billy\",\"LastName\":\"Joel\"}]")
	clone := fromBytes(clonebytes)
	fmt.Printf("Clone: %#v\n", clone2)
    ```
