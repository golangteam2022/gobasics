package main

import (
	"encoding/json"
	"fmt"
)

type Language struct {
	Id   int
	Name string
}

func toBytes(languages []Language) []byte {
	jsonBytes, _ := json.Marshal(languages)
	return jsonBytes
}

func fromBytes(text []byte) []Language {
	var languages []Language
	json.Unmarshal(text, &languages)
	return languages
}

func main() {
	languages := []Language{{1, "english"}}
	text := toBytes(languages)

	fmt.Printf("Objects: %#v\n", languages)
	fmt.Printf("JSON: %q\n", text)

	cloneBytes := []byte("[{\"Id\":1,\"Name\":\"english\"}]")
	clone := fromBytes(cloneBytes)
	fmt.Printf("Clone-Object: %#v\n", clone)
}
