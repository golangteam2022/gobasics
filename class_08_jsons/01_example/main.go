package main

import (
	"encoding/json"
	"fmt"
)

type Human struct {
	FirstName string `json: "firstName"`
	LastName  string `json: "lastName"`
	birthYear int    `json: "birthYear"`
}

func toBytes(humans []Human) []byte {
	jsonBytes, _ := json.Marshal(humans)
	return jsonBytes
}

func fromBytes(text []byte) []Human {
	var humans []Human
	json.Unmarshal(text, &humans)
	return humans
}

func main() {
	persons := []Human{{"Billy", "Joel", 1949}}
	text := toBytes(persons)

	fmt.Printf("%#v\n", persons)

	// bytes can be printed with their decimal values
	fmt.Println(text)
	// or the converted string:
	fmt.Println(string(text))

	// using %q, the bytes or string can directly be printed as the string representation
	fmt.Printf("%q\n", string(text))
	fmt.Printf("%q\n", text)

	// please notice, that the birthYear is missing in the json, since the variable used lowercase letters.
	// so even if the class is parsed from the json string, the birthyear needs to be initiaized with 0
	clone := fromBytes(text)
	fmt.Printf("Clone: %#v\n", clone)

	// we can directly create a json bytearray (parsed from a string) and create the class from it
	clone2bytes := []byte("[{\"FirstName\":\"Billy\",\"LastName\":\"Joel\"}]")
	clone2 := fromBytes(clone2bytes)
	fmt.Printf("Clone: %#v\n", clone2)
}
