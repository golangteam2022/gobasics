# Creating a new project

## Steps

- Step 1: import the 3rd party package `import "github.com/mattetti/goRailsYourself/crypto"` in **main.go**
    ```go
    import (
        "github.com/mattetti/goRailsYourself/crypto"
    )
    ```
- Step 2: This package is unknown to go, and now creates an error message. We need to pull the code with `go get`
    ```bash
	go get github.com/mattetti/goRailsYourself/crypto
    ```

    The expected output is similar to this:
    ```bash
    $ go get github.com/mattetti/goRailsYourself/crypto
    go: downloading github.com/mattetti/goRailsYourself v1.0.0
    go: downloading golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
    go get: added github.com/mattetti/goRailsYourself v1.0.0
    go get: added golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
    ```
- Step 3: you can check now, that the module is available in `$GOPATH/src/github.com/mattetti` or `$GOPATH/pkg/mod/github.com/mattetti/go\!rails\!yourself\@v1.0.0/`:
    ```bash
    ls $GOPATH/src/github.com/mattetti
    ```
    or 
    ```bash
    ls $GOPATH/pkg/mod/github.com/mattetti
    ```

    To add the necessary package `crypto`, run:
    ```bash
    go get github.com/mattetti/goRailsYourself/crypto
    ```
    this will output:
    ```bash
    go get: added github.com/mattetti/goRailsYourself v1.0.0
    go get: added golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
    ```
- Step 4: Print a generated random number and run it!
    ```go
    import (
        "fmt"
        "github.com/mattetti/goRailsYourself/crypto"
    )

    func main() {
        fmt.Println(crypto.GenerateRandomKey(1))
    }
    ```

- Step 5: Confirm that only `Pi` is avaiilable from the `math` package and not `pi`
    ```go
    import (
        "fmt"
        "math"

        "github.com/mattetti/goRailsYourself/crypto"
    )

    func main() {
        fmt.Println(crypto.GenerateRandomKey(1))
        fmt.Println(math.Pi)
    }
    ```

## Info packages

when creating a new library, it is recommended to make it in the fully qualified path, e.g. `github.com/<your username>/<project name>`

## mod and sum files

After the go get, a sum file will be added, which defines the dependencies, needed by golang compiler to build the binary correctly.