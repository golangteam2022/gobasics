package main

import (
	"fmt"

	"github.com/mattetti/goRailsYourself/crypto"
)

func main() {
	fmt.Println(crypto.GenerateRandomKey(1))
}
