package main

import (
	"fmt"
	"os"
	"strconv"
)

var (
	scanned int
)

func main() {
	n, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println("Invalid argument. Please enter a number.")
	}
	fmt.Println("Enter a number: ")
	fmt.Scanln(&scanned)
	fmt.Println(n, "has a square of", square(n))
	fmt.Println(scanned, "has a square of", square(scanned))
}
func square(n int) int {
	return n * n
}
