package main

import (
	"fmt"
)

func add(x int, y int) int {
	return x + y
}

func add_sub(x int, y int) (int, int) {
	return x + y, x - y
}

func main() {
	result := add(42, 13)
	fmt.Println(result)

	result1, result2 := add_sub(42, 13)
	fmt.Println(result1, result2)
}
