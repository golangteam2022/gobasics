package main

import (
	"fmt"
)

var (
	name, location string
	age            int
	points         int = 32
	isVegan        bool
)

const (
	StatusOK = 200
)

func main() {

	greeting := "Hi"

	const NoonTime = 12

	name = "Golang"
	println(name)
	println(age)
	println(points)
	println(isVegan)
	println(greeting)
	println(StatusOK)

	fmt.Println(NoonTime)
}
