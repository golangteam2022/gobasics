# Instructions For Task

- make a program, which takes in a number of floats as arguments, and returns the cumulative sum

- Example:
    ```bash
    go run main.go 1 2 3 4 5
    ```
    expected output
    ```
    [1 3 6 10 15]
    ```

## hints
- using slices may be simpler

- as a reminder on how to parse strings to float with the package `strconv`
    ```go
    strconv.ParseFloat
    ```

- you can iterate over a slice of all passed arguments with
    ```go
    for _, elem := range os.Args[1:] { ... }
    ```

- make a function `cumulative` which takes a slice called `numbers` and modifies its entries to be the cumulative series:
    ```go
    cumulative(numbers []float64) {...}
    ```