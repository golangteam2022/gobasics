package main

import (
	"fmt"
	"os"
	"strconv"
)

func cumulative(numbers []float64) {
	var total float64 = 0
	for idx, elem := range numbers {
		total += elem
		numbers[idx] = total
	}
}

func main() {
	args := make([]float64, 0)
	for _, elem := range os.Args[1:] {
		i, _ := strconv.ParseFloat(elem, 64)
		args = append(args, i)
	}
	cumulative(args)
	fmt.Println(args)
}
