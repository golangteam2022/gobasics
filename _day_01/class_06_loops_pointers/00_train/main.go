package main

import "fmt"

// pass-by-value
func texter(planets [3]string) [3]string {
	for idx, elem := range planets {
		fmt.Println(idx, elem)
		planets[idx] += fmt.Sprintf(" is the %dth planet", idx+1)
	}
	return planets
}

// pass-by-reference
func modifyText(planets *[3]string) {
	for idx, elem := range planets {
		fmt.Println(idx, elem)
		planets[idx] += fmt.Sprintf(" is the %dth planet", idx+1)
	}
}

func main() {
	planets := [3]string{"Mercury", "Venus", "Earth"}
	modifyText(&planets)
	// planets = texter(planets)
	fmt.Println(planets)
}
