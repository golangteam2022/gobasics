# Creating a new project

- Step 1: Created a project inside the examples folder
    ```bash
    cd examples
    go mod init myapp
    ```
- Step 2: Create main.go 
    ```bash
    touch main.go
    ```
- Step 3: Add package info to main go:
    ```go
    package main
    ```
- Step 4: Create a function:
    ```go
    func main() {
        println("Hello World")
    }
    ```
- Step 5: Run the application, using the terminal in the same folder as the go project.
    ```bash
    go run main.go
    ```
- Step 6: Now build the project, e.g. for windows and run it.
    ```bash
    GOOS=windows GOARCH=amd64 go build -o bin/app-amd64.exe main.go
    ```
    or short for the architecture which you are using currently:
    ```bash
    go build .
    ```
    And run it with:
    ```bash
    ./bin/app-amd64.exe
    ```

    To compile for other architectures use one of the following
    ```bash
    
    # Win 32-bit
    GOOS=windows GOARCH=386 go build -o bin/app-386.exe main.go
    
    # Mac 64-bit
    GOOS=darwin GOARCH=amd64 go build -o bin/app-amd64-darwin main.go
    
    # Mac 32-bit
    GOOS=darwin GOARCH=386 go build -o bin/app-386-darwin main.go

    # Linux 64-bit
    GOOS=linux GOARCH=amd64 go build -o bin/app-amd64-linux main.go

    # Linux 32-bit
    GOOS=linux GOARCH=386 go build -o bin/app-386-linux main.go
    ```

- Step 7: (optional) Look at the current env setup with:
    ```bash
    go env
    ```