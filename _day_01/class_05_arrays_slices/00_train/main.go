package main

import "fmt"

func printShoppingList() {
	var shoppingList [4]string
	shoppingList[0] = "Banana"
	shoppingList[1] = "Bread"
	fmt.Println(shoppingList)
	fmt.Println(len(shoppingList))
	fmt.Println(cap(shoppingList))
}

func printPlanets() {
	planets := [3]string{"Mercury", "Venus", "Earth"}
	fmt.Println(planets)
	fmt.Println(len(planets))
	fmt.Println(cap(planets))
}

func printPlanetsImplicitLength() {
	planets := [...]string{"Mercury", "Venus", "Earth", "Mars", "Jupiter"}
	fmt.Println(planets)
	fmt.Println(len(planets))
	fmt.Println(cap(planets))
}

func main() {
	planets := []string{"Mercury", "Venus", "Earth", "Mars", "Jupiter"}
	planets = append(planets, "Saturn")
	planets = append(planets, "Uranus")
	fmt.Println(planets)
	fmt.Println(len(planets))
	fmt.Println(cap(planets))
}
