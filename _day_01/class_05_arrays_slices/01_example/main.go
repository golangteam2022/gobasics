package main

import (
	"fmt"
)

func declareFirstThenSet() {
	var a [2]string
	a[0] = "Hello"
	a[1] = "World"
	fmt.Println(a[0], a[1])
	fmt.Println(a)
}

func declareAndSet() {
	a := [2]string{"hello", "world!"}
	fmt.Printf("%q", a)
}

func declareWithImplicitLength() {
	a := [...]string{"hello", "world!"}
	fmt.Printf("%q", a)
}

func printArrays() {
	a := [2]string{"hello", "world!"}
	fmt.Println(a)
	// [hello world!]
	fmt.Printf("%s\n", a)
	// [hello world!]
	fmt.Printf("%q\n", a)
	// ["hello" "world!"]
}

func appendingToSlices() {
	a := make([]string, 0)
	fmt.Printf("%q\n", a)
	a = append(a, "Hello")
	a = append(a)
	fmt.Printf("%q\n", a)

	a[0] = "Goodbye"
	fmt.Printf("%q\n", a)
	fmt.Println("")
	fmt.Println(cap(a), len(a))
}

func main() {
	appendingToSlices()
}
