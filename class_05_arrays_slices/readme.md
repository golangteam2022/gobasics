# Creating a new project

## Steps

- Step 1: Building on previous knowledge, various examples to create an array, e.g. of strings:
    - declareFirstThenSet
    ```go
    func declareFirstThenSet() {
        var a [2]string
        a[0] = "Hello"
        a[1] = "World"
        // retrieving element by index
        fmt.Println(a[0], a[1])
        fmt.Println(a)
    }
    ```
    - declareFirstThenSet
    ```go
    func declareAndSet() {
        a := [2]string{"hello", "world!"}
        fmt.Printf("%q", a)
    }
    ```
    - declare with implicit length
    ```go
    func declareWithImplicitLength() {
        a := [...]string{"hello", "world!"}
        fmt.Printf("%q", a)
    }
    ```
- Step 2: Print the array
    ```go
    func printArrays() {
        a := [2]string{"hello", "world!"}
        fmt.Println(a)
        // [hello world!]
        fmt.Printf("%s\n", a)
        // [hello world!]
        fmt.Printf("%q\n", a)
        // ["hello" "world!"]
    }
    ```
- Step 3: appending and reassigning slices:
    ```go
    func appendingToSlices() {
        a := make([]string, 0)
        a = append(a, "Hello")
        a = append(a, "World")
        fmt.Printf("%q\n", a)

        a[0] = "Goodbye"
        fmt.Printf("%q\n", a)
    }
    ```
- Step 4: call the functions and run it.

## Info

While arrays have fixed length and cannot grow, slices can be expanded and can grow.


https://gobyexample.com/arrays

https://gobyexample.com/slices

