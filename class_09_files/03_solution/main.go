package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Salary struct {
	Basic, HRA, TA float64
}

type Employee struct {
	FirstName, LastName, Email string
	Age                        int
	MonthlySalary              []Salary
}

func main() {
	jsonFilePath := "salaries.json"
	jsonFile, _ := os.Open(jsonFilePath)
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var employee Employee
	json.Unmarshal(byteValue, &employee)
	fmt.Println(employee)
}
