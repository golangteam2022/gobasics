package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Users struct {
	Users []User `json:"users"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"Age"`
	Social Social `json:"social"`
}

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

func fromFile(jsonFilePath string) Users {
	// read our opened jsonFile as a byte array.
	jsonFile, err := os.Open(jsonFilePath)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Successfully Opened " + jsonFilePath)
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	// we initialize our Users array
	var users Users

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &users)
	return users
}

func toFile(filename string, data Users) {
	file, _ := json.MarshalIndent(data, "", " ")
	_ = ioutil.WriteFile(filename, file, 0644)
	fmt.Println("Successfully wrote " + filename)
	// alternatively, a file can also be written to by passing a string
	// file, _ := os.Open("test.txt")
	// len, err := file.WriteString("Welcome to GeeksforGeeks."+
	// " This program demonstrates reading and writing"+
	// 			 " operations to a file in Go lang.")

}

func main() {
	users := fromFile("users.json")
	fmt.Println(users)
	toFile("new_users.json", users)
}
