package main

import "fmt"

func forLoop() {
	for idx, elem := range [...]int{10, 20, 30, 40, 50} {
		fmt.Println(idx, elem)
	}
}

func makeSquares(array [10]int) {
	for idx, elem := range array {
		array[idx] = elem * elem
	}
}

func makeSquaresInplace(array *[10]int) {
	for idx, elem := range array {
		array[idx] = elem * elem
	}
}

func main() {
	// first for loop example
	forLoop()

	a := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	// pass by value
	// passing the array, passes a full copy of the array
	// so the original array will not be changed
	makeSquares(a)
	fmt.Println(a)

	// pass by reference
	// however if specifically the pointer is passed,
	// then the values of the array are changed
	makeSquaresInplace(&a)
	fmt.Println(a)
}
