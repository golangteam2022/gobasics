package main

import (
	"fmt"
	"os"
	"strconv"
)

func cumulative(numbers []float64) {
	sum := 0.
	for idx, elem := range numbers {
		sum += elem
		numbers[idx] = sum
	}
}

func main() {
	numbers := []float64{}
	for _, elem := range os.Args[1:] {
		number, _ := strconv.ParseFloat(elem, 64)
		numbers = append(numbers, number)
	}
	cumulative(numbers)
	fmt.Println(numbers)
}
