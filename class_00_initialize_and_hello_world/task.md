# Instructions For Task

- navigate to the exercise folder with a terminal, 
    and initiate a project with `go mod`

- Create a Go script which prints your name.

- Build the project, and execute the binary.


