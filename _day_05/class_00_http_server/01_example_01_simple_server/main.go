// HTTP server example
package main

import (
	"fmt"
	"log"
	"net/http"
)

// helloHandler returns a greeting
func helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello Gophers!")
}

type apiHandler struct{}

func (apiHandler) ServeHTTP(http.ResponseWriter, *http.Request) {}

func servingWithHTTP() {
	// Serving a http path means, that the main go routine loop branches of another go routine which handles
	// the request, either with a struct that implements
	// func (apiHandler) ServeHTTP(http.ResponseWriter, *http.Request) {}
	// or by directly refering to a function.

	http.HandleFunc("/hello", helloHandler) // both use defaultServerMux
	http.Handle("/api/", apiHandler{})
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

func ServeViaNewServerMux() {
	mux := http.NewServeMux()
	mux.Handle("/api/", apiHandler{})
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		// The "/" pattern matches everything, so we need to check
		// that we're at the root here.
		if req.URL.Path != "/" {
			http.NotFound(w, req)
			return
		}
		fmt.Fprintf(w, "Welcome to the home page!")
	})
}

func main() {
	servingWithHTTP()
	// ServeViaNewServerMux()
}
