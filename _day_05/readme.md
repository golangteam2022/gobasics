Day 5

- create own package, own module https://github.com/PatrickVienne/go-vectors
- testing https://www.digitalocean.com/community/tutorials/how-to-write-unit-tests-in-go-using-go-test-and-the-testing-package
- ci/cd, unittests, dockerization https://gitlab.com/PatrickVienne/go-docker-cicd
- testing https://pkg.go.dev/testing@go1.17.8
- GOROOT, GOPATH https://golang.co/difference-between-gopath-and-goroot/
- profiling (pprof) https://archive.fosdem.org/2014/schedule/event/hpc_devroom_go/attachments/slides/486/export/events/attachments/hpc_devroom_go/slides/486/FOSDEM14_HPC_devroom_14_GoCUDA.pdf
- benchmarking (-bench)
- http, net, mux
- env variables https://tutorialedge.net/golang/working-with-environment-variables-in-go/
- go context https://tutorialedge.net/golang/go-context-tutorial/
- simple rest-api, deployed on heroku, with postgresql and gorm (https://www.youtube.com/watch?v=qeElSTaEB6Y&ab_channel=devtopics)

- go templating