# Unittest and benchmark


## Tests

Files should end with `_test.go`. E.g. `sqrt_test.go`


TestFunctions should start with `Test`. E.g. `TestSimple`

```go
func TestSimple(t *testing.T) {
	val, err := Sqrt(2)

	if err != nil {
		t.Fatalf("error in calculation - %s", err)
	}

	if !almostEqual(val, 1.414214) {
		t.Fatalf("bad value - %f", val)
	}
}
```

You can start this by
```bash
go test
```

## Benchmark

To create benchmark tests, declare a function starting with `Benchmark`

```go
func BenchmarkSqrt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := Sqrt(float64(i))
		if err != nil {
			b.Fatal(err)
		}
	}
}
```

and run the benchmark test with:

`go test -bench=.`

## Filter

to only run tests of Sqrt.

```go
go test -run=Sqrt -bench=.
```

## Multiple Benchmarks

```go
func benchmarkCalculate(input int, b *testing.B) {
    for n := 0; n < b.N; n++ {
        Calculate(input)
    }
}

func BenchmarkCalculate100(b *testing.B)         { benchmarkCalculate(100, b) }
func BenchmarkCalculateNegative100(b *testing.B) { benchmarkCalculate(-100, b) }
func BenchmarkCalculateNegative1(b *testing.B)   { benchmarkCalculate(-1, b) }
```

We can run all these 3 with:

```bash
go test -run=Bench -bench=.
```

## Profiling

### CPU

Run the tests and create a cpuprofile output

```bash
go test -cpuprofile cpu.prof -bench .
```

It is possible to open this with the `pprof` command

```bash
go tool pprof cpu.prof
```

Now it is possible to look up the top 5 calls, in the pprof CLI

```bash
top5 -cum
```


### Memory

	


```bash
go test -memprofile mem.prof -bench .
```

It is possible to open this with the `pprof` command

```bash
go tool pprof cpu.prof
```

Now it is possible to look up the top 5 calls, in the pprof CLI

```bash
top5 -cum
```


## Combine functions

### Inefficient Fibonacci


The test file:
```go
// main_test.go
package main
 
import "testing"
 
func TestGetVal(t *testing.T) {
    for i := 0; i < 1000; i++ {             // running it a 1000 times
        if Fib2(30) != 832040 {
            t.Error("Incorrect!")
        }
    }
}
```

### Implementation 1
Recursive implementation
```go
// main.go
package main

func Fib2(n int) uint64 {
    if n == 0 {
        return 0
    } else if n == 1 {
        return 1
    } else {
        return Fib2(n-1) + Fib2(n-2)
    }
}
 
func main() {
    // fmt.Println(Fib2(30)) // 832040
}
```

### Implementation 2
Implementation without recursive code
```go
var f [100010]uint64
 
func Fib(n int) uint64 {
    f[0] = 0
    f[1] = 1
 
    for i := 2; i <= n; i++ {
        f[i] = f[i-1] + f[i-2]
    }
 
    return f[n]
}
```

### Implementation 3
Huge allocation to show in the profiler
```go
func Fib(n int) uint64 {
 
    f := make([]uint64, 10000)       // make huge allocation
 
    f[0] = 0
    f[1] = 1
 
    for i := 2; i <= n; i++ {
        f[i] = f[i-1] + f[i-2]
    }
 
    return f[n]
}
```

## Sources

3 Parts:
- https://golangdocs.com/golang-unit-testing
- https://golangdocs.com/profiling-in-golang
- https://golangdocs.com/profiling-in-golang