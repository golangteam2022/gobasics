package main

import (
	"flag"
	"fmt"
)

func main() {
	// run with
	// go run main.go -language german -n 10
	language := flag.String("language", "", "add a language to use")

	// get the maximum number of messages from flags
	seconds := flag.Int("n", 5, "defines seconds tolerance")

	flag.Parse()

	fmt.Println(*seconds, *language)
}
