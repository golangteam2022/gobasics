package ex2

import "bytes"

type Viper struct {
	// just placeholder
	config interface{}
}

func (v *Viper) marshalReader(b *bytes.Buffer, inter interface{}) {
	// just placeholder
}

// this is the issues
func (v *Viper) ReadBufConfig(buf *bytes.Buffer) error {
	v.config = make(map[string]interface{})
	v.marshalReader(buf, v.config)
	return nil
}
