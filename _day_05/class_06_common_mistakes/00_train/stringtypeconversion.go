package main

import (
	"fmt"
	"strconv"
)

func main() {
	i := 105
	s := string(i)
	// What output do you expect?
	fmt.Println(s)
}

// The output is: i
// the string(i) regards 105 as a rune, and 105 is the ascii code for i
// use strconv for conversions.Itoa
// or fmt.Sprintf("The integer is: %d", a)
// or strconv.FormatInt(int64(a), 10)     // 1234

func Solution() {
	a := 1234
	fmt.Println(strconv.Itoa(a)) // 1234
}
