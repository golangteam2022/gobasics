package typeconversion

import "fmt"

func main() {
	var x, y = 13, 3.5
	// what is the issue here?
	fmt.Println(x / y)

	// why could this work instead?
	// var x = 13 / 3.5
	// fmt.Println(x)
}

// SOL: the types of constants are set once the constants are used the first time.
// In the above program, the right side of = are constants, not variables.
// Hence compiler will convert 13 into a float type to execute the program. This program will print the output
