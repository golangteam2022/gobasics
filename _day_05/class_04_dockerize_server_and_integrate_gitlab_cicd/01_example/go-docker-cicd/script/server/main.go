package main

import (
	"encoding/json"
	"fmt"
	"go-micro-server/utils/calc"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()

	// router handler / endpoints
	router.HandleFunc("/api/v1/calc/{op}", calculator).Methods("GET")
	router.HandleFunc("/api/v1/heroes", getHeroes).Methods("GET")
	router.HandleFunc("/api/v1/heroes", postHeroes).Methods("POST")

	// listen and server the router with the port
	port := os.Getenv("PORT")
	if port == "" {
		log.Panicf("Could not find PORT in environment variables")
	} else {
		log.Printf("Serving on Port: " + port)
	}

	http.ListenAndServe(":"+port, router)
}

func getHeroes(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint One")
	w.Write([]byte(`Heroes`))
}

func postHeroes(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint Two")
	w.Write([]byte(`Heroes`))
}

func calculator(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	operation := params["op"]
	if operation == "add" {
		values := []byte(r.FormValue("values"))
		var data []int
		json.Unmarshal(values, &data)
		result := calc.Adder(data...)
		w.Write([]byte(fmt.Sprintf("Adder(%v) = %d", data, result)))
	} else {
		w.Write([]byte(`Unknown Operation`))
	}
}
