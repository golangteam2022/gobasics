# Go Docker Cicd

Example project showcasing unit tests and cicd for a golang project, including dockerization

## start:

```
PORT=8080 go run script/server/main.go
```

or after build:

```
go build script/server/main.go
PORT=8080 ./main.exe
```

or start with docker:
```
docker-compose up --build
```

Output:
```
Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
Recreating go-docker-cicd_web_1 ... done
Attaching to go-docker-cicd_web_1
web_1  | 2022/03/06 23:48:30 Serving on Port: 8080
```

After this, you can test it by opening:

http://localhost:8080/api/v1/calc/add?values=[1,2,3,4,70]

The website should output
```
Adder([1 2 3 4 70]) = 80
```