# Use published module on github

Define a package on github or gitlab and reuse for the project.

Example:

github.com/PatrickVienne/go-vectors

show all versions:
```
go list -u -m all
```

```bash
$ go list -u -m all
learn
github.com/PatrickVienne/go-vectors v0.0.0-20220301175750-4bd12df1f6af [v0.0.0-20220301180815-11910c45195c]
```

Updating to the latest version
```bash
$ go get -u github.com/PatrickVienne/go-vectors
go: downloading github.com/PatrickVienne/go-vectors v0.0.0-20220301180815-11910c45195c
go get: upgraded github.com/PatrickVienne/go-vectors v0.0.0-20220301175750-4bd12df1f6af => v0.0.0-20220301180815-11910c45195c
```
## addtional tip on tests:

https://archive.fosdem.org/2014/schedule/event/hpc_devroom_go/attachments/slides/486/export/events/attachments/hpc_devroom_go/slides/486/FOSDEM14_HPC_devroom_14_GoCUDA.pdf


## Profiling with Benchmark tests
```
go test -cpuprofile cpu.prof -bench=.
```

And the benchmarks will be run and create a prof file with filename cpu.prof (in the above example).


```
go tool pprof cpu.prof
```

Then within the golang profiler tool, you can use the commands:
```
(pprof) top
```

To list all processes narrowed by a regex search
```
(pprof) peek run*
```

To create a visualization with dot, graphviz needs to be installed.
Then you can visualize the call graph in a browser with the web commands
```
(pprof) web
```

## cgo

Additional Info about integration of C into Go

https://sodocumentation.net/go/topic/6125/cgo
