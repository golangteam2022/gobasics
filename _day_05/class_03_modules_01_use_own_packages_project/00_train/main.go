package main

import (
	"fmt"

	calc "github.com/PatrickVienne/go-vectors/calc"
)

func main() {
	fmt.Println(calc.Dot([]float64{1.0, 2.0}, []float64{1.0, 2.0}))
}
