# Mux Server with Gorm and Persistence


```bash
	go get gorm.io/gorm
```

Installing sqlite might on windows might give the following error:
```
$ go get -u github.com/mattn/go-sqlite3
# github.com/mattn/go-sqlite3
cgo: C compiler "gcc" not found: exec: "gcc": executable file not found in %PATH% 
```

Resolution:
1. Download and install gcc here: https://jmeubank.github.io/tdm-gcc/
2. After installation (e.g. in the path: `C:\Tools\gcc\bin"`), you can set the compiler path for go for the installation command: ```CC=/c/Tools/tdm-gcc/bin/gcc.exe go get -u github.com/mattn/go-sqlite3```
This will give back
```
go get: upgraded github.com/mattn/go-sqlite3 v1.14.9 => v1.14.12
```
3. Then you can run the install: `CC=/c/Tools/tdm-gcc/bin/gcc.exe go install github.com/mattn/go-sqlite3`

## Starting postgresdb in a docker

Install docker if you do now have it yet:
https://www.docker.com/products/docker-desktop


With the `docker-compose.yml` setup, you only need to run
```bash
docker-compose up
```

Or, if you want to run it in the background, use
```bash
docker-compose up -d
```

## Environment variables:

For running a script while taking the `.env` file into account, following line is an example:
```bash
env $(grep -vE "^(#.*|\s*)$" .env ) go run main.go
```

## Database Browser for postgresdb

dbeaver is a free dbrowser for many different dbs:

https://dbeaver.io/download/

After starting the application, check for the entries in the database