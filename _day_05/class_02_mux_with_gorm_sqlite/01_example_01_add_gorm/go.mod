module main

go 1.17

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/sirupsen/logrus v1.8.1
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/mattn/go-sqlite3 v1.14.0 // indirect
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
)
