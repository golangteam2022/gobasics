/*
Using the GORM ORM in Go.
*/
package main

import (
	"fmt"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	// _ "github.com/jinzhu/gorm/dialects/sqlite"
	log "github.com/sirupsen/logrus"
)

type (
	User struct {
		gorm.Model
		Name     string
		Username string `gorm:"not null"`
		Password string `gorm:"not null"`
		Messages []Message
	}
	Profile struct {
		TZ *time.Location
	}
	Message struct {
		Body   string `gorm:"not null`
		User   User
		UserID uint
		gorm.Model
	}
)

func main() {
	log.Info("Connecting to SQL Db...")
	ap := os.Getenv("POSTGRES_PORT")
	log.Info("Port", ap)
	port, err := strconv.Atoi(ap)
	if err != nil {
		log.Fatalf("POSTGRES_PORT environment variable not set")
	}
	dsn := url.URL{
		User:     url.UserPassword(os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD")),
		Scheme:   "postgres",
		Host:     fmt.Sprintf("%s:%d", os.Getenv("POSTGRES_HOST"), port),
		Path:     os.Getenv("POSTGRES_DB"),
		RawQuery: (&url.Values{"sslmode": []string{"disable"}}).Encode(),
	}

	db, err := gorm.Open("postgres", dsn.String())

	// db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.AutoMigrate(&User{})
	db.AutoMigrate(&Profile{})
	db.AutoMigrate(&Message{})

	log.Info("Database: %#v\n", db)

	var user = &User{Name: "Peter Jones", Username: "pj@email.com", Password: "password"}
	// CRUD = create, retrieve, update, and delete
	if err := db.Model(&User{}).Create(user).Error; err != nil {
		log.Warn(err)
	}
	users, _ := db.Model(&User{}).Find(user).Rows()
	for users.Next() {
		u := new(User)
		db.ScanRows(users, u)
		fmt.Printf("Got: Name: %v, Username: %v\n", u.Name, u.Username)
	}

	mesgs := []*Message{
		&Message{Body: "My message 1"},
		&Message{Body: "My message 2"},
		&Message{Body: "My message 3"},
	}

	db.Model(user).Find(user)
	for _, m := range mesgs {
		db.Model(user).Association("Messages").Append(m)
	}
}
