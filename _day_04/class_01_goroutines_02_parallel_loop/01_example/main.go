package main

import (
	"fmt"
)

var (
	routes = []struct {
		url  string
		path string
	}{
		{"http://example.com/1", "/1"},
		{"http://example.com/2", "/2"},
		{"http://example.com/3", "/3"},
		{"http://example.com/4", "/4"},
		{"http://example.com/5", "/5"},
	}
)

func NotThreadsafe() {
	fmt.Println("==NotThreadsafe==")
	// will definitely be detected by
	// go run -race main.go
	for _, tt := range routes {
		go func() {
			fmt.Println(tt.url) // Not guaranteed which item will be stored in tt during the goroutine execution.
		}()
	}
}

func ThreadSafe1() {
	fmt.Println("==ThreadSafe1==")
	for _, tt := range routes {
		// pass tt.url directly
		go func(url string) {
			fmt.Println(url)
		}(tt.url)
	}
}

func ThreadSafe2() {
	fmt.Println("==ThreadSafe2==")
	for _, tt := range routes {
		// make a local copy of tt
		tt := tt
		go func() {
			fmt.Println(tt.url)
		}()
	}
}

func main() {
	NotThreadsafe()
	ThreadSafe1()
	ThreadSafe2()
}
