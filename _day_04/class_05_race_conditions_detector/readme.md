# Go race detector

https://go.dev/blog/race-detector

https://go.dev/doc/articles/race_detector

## Intro
```
Race conditions are among the most insidious and elusive programming errors. They typically cause erratic and mysterious failures, often long after the code has been deployed to production. While Go’s concurrency mechanisms make it easy to write clean concurrent code, they don’t prevent race conditions. Care, diligence, and testing are required. And tools can help.

We’re happy to announce that Go 1.1 includes a race detector, a new tool for finding race conditions in Go code. It is currently available for Linux, OS X, and Windows systems with 64-bit x86 processors.

The race detector is based on the C/C++ ThreadSanitizer runtime library, which has been used to detect many errors in Google’s internal code base and in Chromium. The technology was integrated with Go in September 2012; since then it has detected 42 races in the standard library. It is now part of our continuous build process, where it continues to catch race conditions as they arise.
```

How it works
```
The race detector is integrated with the go tool chain. When the -race command-line flag is set, the compiler instruments all memory accesses with code that records when and how the memory was accessed, while the runtime library watches for unsynchronized accesses to shared variables. When such “racy” behavior is detected, a warning is printed. (See this article for the details of the algorithm.)

Because of its design, the race detector can detect race conditions only when they are actually triggered by running code, which means it’s important to run race-enabled binaries under realistic workloads. However, race-enabled binaries can use ten times the CPU and memory, so it is impractical to enable the race detector all the time. One way out of this dilemma is to run some tests with the race detector enabled. Load tests and integration tests are good candidates, since they tend to exercise concurrent parts of the code. Another approach using production workloads is to deploy a single race-enabled instance within a pool of running servers.
```

## Info
Using it:
```bash
go test -race mypkg    // test the package
go run -race mysrc.go  // compile and run the program
go build -race mycmd   // build the command
go install -race mypkg // install the package
```


Example which has race condition
```go
package main

import "fmt"

func main() {
	done := make(chan bool)
	m := make(map[string]string)
	m["name"] = "world"
	go func() {
		m["name"] = "data race"
		done <- true
	}()
	fmt.Println("Hello,", m["name"])
	<-done
}

```

Output

```
$ go run -race main.go 
Hello, world
==================
WARNING: DATA RACE
Write at 0x00c000142450 by goroutine 7:
  runtime.mapassign_faststr()
      C:/Program Files/Go/src/runtime/map_faststr.go:202 +0x0
  main.main.func1()
      C:/code/gitlab.com/golangteam2022/gobasics/class_13_/01_example/main.go:10 +0x50

Previous read at 0x00c000142450 by main goroutine:
  runtime.mapaccess1_faststr()
      C:/Program Files/Go/src/runtime/map_faststr.go:12 +0x0
  main.main()
      C:/code/gitlab.com/golangteam2022/gobasics/class_13_/01_example/main.go:13 +0x16d

Goroutine 7 (running) created at:
  main.main()
      C:/code/gitlab.com/golangteam2022/gobasics/class_13_/01_example/main.go:9 +0x150
==================
```