package select 

func Fanout(In <-chan int, OutA, OutB, chan int) {
	// Could also call this load balancer
	for data := range In {  // receive until closed
		select {  // sends to first non-blocking channel
		case OutA<-data:
		case OutB<-data:
		}
	}
}

func Turnout(Quit <-chan int, InA, InB, OutA, OutB, chan int) {
	for {  // forever
		select {  // receive from 1st non-blocking
		case OutA, more <- InA:
		case OutB, more <- InB:

		case <- Quit:
			close(InA)  // this will not call the case on top, since first this select case has to be processed until the end.
			close(InB)  // however, this still is an antipattern

			Fanout(InA, OutA, OutB)
			Fanout(InB, OutA, OutB)
			return
		}
	}
}

