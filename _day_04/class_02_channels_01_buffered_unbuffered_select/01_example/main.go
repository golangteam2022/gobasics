package main

import (
	"fmt"
	"time"
)

func BlockedReceiver() {
	unbuffered := make(chan int)
	// blocks, because nobody adds a value to the unbuffered channel
	a := <-unbuffered
	fmt.Println(a)
}

func BlockedSender() {
	unbuffered := make(chan int)
	// blocks, because nobody takes the value from the channel
	unbuffered <- 1
	fmt.Println(unbuffered)
}

func SyncStep() {
	unbuffered := make(chan int)
	go func() {
		// this starts, and blocks until the channel receives an entry.
		// the entry is then taken, this will unblock.
		<-unbuffered
	}()
	unbuffered <- 1
	fmt.Println(unbuffered)
}

func BlockedBuffered() {
	buffered := make(chan int, 1)
	buffered <- 1
	buffered <- 2
	fmt.Println(buffered)
}

func ClosingChannel() {
	c := make(chan int)
	close(c)
	// closing generates a message
	fmt.Println(<-c)
}

func TryReceiveWithoutTimeout(c chan int) (data int, more, ok bool) {
	select {
	case data, more = <-c:
		return data, more, true
	default: // done when c is blocking
		return 0, true, true
	}
}

func TryReceiveWithTimeout(c chan int, duration time.Duration) (data int, more, ok bool) {
	select {
	case data, more = <-c:
		return data, more, true
	case <-time.After(duration): // time.After returns a channel, which gets a message sent after the timeout.
		return 0, true, true
	}
}

func main() {
	// Channel examples
	// BlockedReceiver()
	// BlockedSender()
	ClosingChannel()

	// Example with timeout
	/*
		unbuffered := make(chan int)
		go func() {
			time.Sleep(500 * time.Millisecond)
			// time.Sleep(1500 * time.Millisecond)
			// unbuffered <- 10
			close(unbuffered)
		}()
		data, more, ok := TryReceiveWithTimeout(unbuffered, time.Second)
		fmt.Println(data, more, ok)
	*/
}
