package main

import (
	"bufio"
	"log"
	"net/http"
	"strings"

	// "sync"

	"golang.org/x/time/rate"
)

var (
// wg sync.WaitGroup
)

// NO need to change this function
func GetWebsiteData(retrievedData chan<- string) {
	res, err := http.Get("https://httpbin.org/delay/1")
	if err != nil {
		log.Fatal(err)
	}
	// defer wg.Done()
	defer res.Body.Close()

	// option 1, using scanner
	b := strings.Builder{}
	scanner := bufio.NewScanner(res.Body)
	for scanner.Scan() {
		// without newlines
		b.Write(scanner.Bytes())

		// with new lines added back
		// b.Write(append(scanner.Bytes(), '\n'))

		// or
		// b.WriteString(scanner.Text())
	}
	data := b.String()

	// option 2, using ioutil
	// body, err := ioutil.ReadAll(res.Body)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// data := string(body)

	retrievedData <- data

}

func RequestWithLimit(retrievedData chan<- string, concurrentCalls int, rateLimit rate.Limit, burst int) {
	/*
		Implement an algorithm, which has at most the given concurrent calls running,
		and overall follows the rate limit

		The retrieved data should be written to the retrievedData channel
	*/
}

func main() {
	// // suggestions to start:
	// retrievedData := make(chan string)
	// concurrentRequests := 3
	// burstLimit := 5
	// limitFreq := rate.Every(1 * time.Second)
	// maxPackage := 30
	// fmt.Println("== START WEBREQUESTS ==")
	// fmt.Printf("== CONCURRENT: %d ==\n", concurrentRequests)

	/*

		start the RequestWithLimit function in a go routine

		get the first 30 data packages,
		print a counter which counts to 30 with each package,
		and print the number of bytes

		after 30 packages arrive. close the channel
	*/

	log.Println("==========")
}
