# Task

Write a Code which will first ask, how old the user is.

The user should respond by typing a number.

If the user is less than 4 years old, output: "You are quite young"
If the user is an even number of years but less than 75, output: "Your age is special"
If the user is between 10 and 18 (inclusive), output: "You're growing fast"
If the user is older than 18 but younger than 75: "This world is now in your hands."
If the user is older than 75: "Thank you for all your contributions."
In any other case, it should output: "Enjoy this year."

For scanning the user input, you can use this snippet:

```go
	var age uint64
	fmt.Scanf("%d", &age)
```