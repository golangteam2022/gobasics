# Control Flow

## switch

Switch statements allow a simple grouping and directing of the execution based on certain values.
In general, each case is a specific that should be executed only for the given conditions, however there are ways to combine multiple cases, as we will see.

### Simple case

The simplest example needs a switch and a case.

E.g. we check what date is today, and output a text accordingly.

```go
	today := time.Now()
	switch today.Day() {
	case 5: // block for value being 5
		fmt.Println("Today is 5th. Clean your house.")
```

### Grouped cases

Switch also allows to group multiple cases in one line:

E.g. we have the same execution for days 10th, 11th and 12th
```go
	case 10, 11, 12: // select common block of code for many similar cases.
		fmt.Println("Today is 10th/11th/12th. Exercise.")
```

### `fallthrough` keyword

We can also let a case continue with more cases, by using the `fallthrough` keyword.

In the following example, The code will print:
```
Buy some wine.
Today is 15th. Visit a doctor.
```
The code is:
```go
	myNumber := 13

	switch myNumber {
	case 13:
		fmt.Println("Buy some wine.")
		fallthrough // keyword used to force the execution flow to fall through the successive case block.
	case 15:
		fmt.Println("Today is 15th. Visit a doctor.")
```

### `default` keyword

If a value in the select does not match any case, a `default` can be defined to handle all other cases:

```go
	myNumber := 20

	switch myNumber {
	case 13:
		fmt.Println("Buy some wine.")
		fallthrough // keyword used to force the execution flow to fall through the successive case block.
	case 15:
		fmt.Println("Today is 15th. Visit a doctor.")
	default:
		fmt.Println("Nothing Special.")
```

### initializing variables in switch

just like in `if` statements, it is possible to also initialize variables and then reuse them in the `case` statements.

E.g. we can say, that the house should be cleaned each 5fth day of a month.

```go
	switch today := time.Now(); {
	case today.Day() < 5:
		fmt.Println("Clean your house.")
```

### `break` statement

Break statements usually interrupt the flow and immediately exit the switch block, however in general they are only necessary, if this has to be determined on runtime within a case, since in general the switch will only run 1 case, even if a criteria is fulfullied by more cases:

```go
	myNumber := 4

	switch {
	case myNumber < 5:
		fmt.Println("Clean your house.")
	case myNumber <= 10:
		fmt.Println("Buy some wine.")
```

This will only print:
```
Clean your house.
```

func main() {

}
