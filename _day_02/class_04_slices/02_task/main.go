package main

import (
	"fmt"
)

func GetNewMixedRandomQuote(quotes [][]string) string {
	/* TODO randomly choose 2 quotes */

	/* TODO the 2 quotes must be different ones */

	/* TODO concat the first half of the first with the second half of the second */

	/* TODO return the final new inspirational quote */
	return ""
}

func main() {
	// quotes are taken from: https://type.fit/api/quotes

	var laoTzu = []string{"Doing", "nothing", "is", "better", "than", "being", "busy", "doing", "nothing"}
	var confuscius = []string{"Study", "the", "past", "if", "you", "would", "divine", "the", "future."}
	var freud = []string{"From", "error", "to", "error", "one", "discovers", "the", "entire", "truth."}
	var benjaminFranklin = []string{"Well", "done", "is", "better", "than", "well", "said."}
	var napoleonHill = []string{"Ideas", "are", "the", "beginning", "points", "of", "all", "fortunes."}

	newQuote := GetNewMixedRandomQuote([][]string{laoTzu, confuscius, freud, benjaminFranklin, napoleonHill})
	fmt.Println(newQuote)
}
