package main

import (
	"fmt"
	"time"
)

func main() {
	// initializing
	prices := map[string]float64{
		"pancake":    1.64,
		"strawberry": 2.20, // last comma is a must
	}

	// retrieving values
	fmt.Println("==maps get values==")
	pancakePrice := prices["pancake"]
	fmt.Println("pancakePrice", pancakePrice) //pancakePrice 1.64

	strawberryPrice, strawberryExists := prices["strawberry"]
	fmt.Println("strawberryPrice, strawberryExists: ", strawberryPrice, strawberryExists) //strawberryPrice, strawberryExists:  2.2 true

	blueberryPrice, blueberryExists := prices["blueberry"]
	fmt.Println("blueberryPrice, blueberryExists", blueberryPrice, blueberryExists) //blueberryPrice, blueberryExists 0 false

	fmt.Println(prices, "len:", len(prices)) //map[pancake:1.64 strawberry:2.2] len: 2
	delete(prices, "pancake")
	fmt.Println(prices, "len:", len(prices)) //map[strawberry:2.2] len: 1

	// simple increments
	fmt.Println("==increments==")
	sold := map[string]uint64{}
	soldPancakes := sold["pancake"]
	fmt.Println("soldPancakes", soldPancakes)
	sold["pancake"]++
	soldPancakes = sold["pancake"]
	fmt.Println("soldPancakes", soldPancakes)

	fmt.Println("==changing lists==")
	// having lists
	friends := map[string][]string{}
	friends["John"] = []string{"Mary"}
	johnFriends := friends["John"]
	fmt.Println("johnFriends", johnFriends) //johnFriends [Mary]
	// this will not modify the map
	johnFriends = append(johnFriends, "Sally")
	johnFriends = friends["John"]
	fmt.Println("johnFriends", johnFriends) //johnFriends [Mary]

	johnFriends = append(johnFriends, "Sally")
	friends["John"] = johnFriends
	johnFriends = friends["John"]
	fmt.Println("johnFriends", johnFriends) //johnFriends [Mary Sally]

	// for merging maps,
	// the map with the updates has to be iterated through to write all new values onto the old map
	fmt.Println("==merge maps==")
	MergeMaps()

	// unsafe example for using maps
	fmt.Println("==unsafe maps==")
	unsafeMapUse()
	time.Sleep(500 * time.Millisecond)
}

var (
	allData = make(map[string]string)
)

func get(key string) string {
	val := allData[key]
	fmt.Println("get k", key, val)
	return val
}

func set(key string, value string) {
	allData[key] = value
	fmt.Println("set key/value", key, value)
}

func unsafeMapUse() {
	go set("a", "Some Data 1")
	go set("b", "Some Data 2")
	go get("a")
	go get("b")
	go get("a")
}

func MergeMaps() {
	first := map[string]int{"a": 1, "b": 2, "c": 3}
	second := map[string]int{"a": 1, "e": 5, "c": 3, "d": 4}

	for k, v := range second {
		first[k] = v
	}

	fmt.Println(first)
}
