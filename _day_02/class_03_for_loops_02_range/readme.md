# For loops - Range

The second way of using for loop, is with the range keyword.

It enables use to loop every anything that is iterable.

## Iterate over maps

To iterate over maps, there are 2 ways.

Let's say we have a dictionary mapping countries to their capitals.

### Option 1: iterate over keys

The first way is to just iterate over all the keys:

```go
	countryToCapital := map[string]string{"Japan": "Tokyo", "China": "Beijing", "Canada": "Ottawa"}
	for country := range countryToCapital {
		fmt.Println("Country: ", country, "Capital: ", countryToCapital[country])
	}
```

### Option 2: iterate over keys and values

The 2nd way is to get key and value in each iteration, and thereby iterate through each key and value
```go
	countryToCapital := map[string]string{"Japan": "Tokyo", "China": "Beijing", "Canada": "Ottawa"}
	for country, capital := range countryToCapital {
		fmt.Println("Country: ", country, "Capital: ", countryToCapital)
	}
```

### Option 3: iterate over values

If you only need the values, you can also tell go to ignore the keys by placing an `_`:
```go	
	for _, value := range myMap {
		fmt.Println(value)
	}
```

## Iterate over slices and arrays

For arrays and slices iteration works the same way, except the key of a map is now the index of the array.

So our 3 cases are:

### Option 1: iterate over index

The first way is to just iterate over all the keys:

```go
	myArray := []int{1, 10, 20, 40, 50, 60}
	for idx := range myArray {
		fmt.Println("My Slice", idx, myArray[idx])
	}
```

### Option 2: iterate over index and values

```go
	for idx, val := range []int{1, 10, 20, 40, 50, 60} {
		fmt.Println("My Slice", idx, val)
	}
```

### Option 3: iterate over values

```go
	for _, val := range []int{1, 10, 20, 40, 50, 60} {
		fmt.Println("My Slice", val)
	}
```

## Walker functions

you can use interfaces wo make a walker function, which can iterate through an array and a map:

```go
func walk(v interface{}) {
    switch v := v.(type) {
    case []interface{}:
        for i, v := range v {
            fmt.Println("index:", i)
            walk(v)
        }
    case map[interface{}]interface{}:
        for k, v := range v {
            fmt.Println("key:", k)
            walk(v)
        }
    default:
        fmt.Println(v)
    }
}
```