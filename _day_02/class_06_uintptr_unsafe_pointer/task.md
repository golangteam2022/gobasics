# Instructions For Task

- make structs which represent the json content

- you will need multiple structs to represent the nested format of the json

- create a script which can read the json and print its output to the terminal

## hint

https://www.golangprograms.com/golang-writing-struct-to-json-file.html
