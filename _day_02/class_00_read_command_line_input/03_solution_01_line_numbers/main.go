package main

import (
	"bufio"
	"fmt"
	"os"
)

func addFilenumberToFile() {
	scanner := bufio.NewScanner(os.Stdin) // by default splits at newline characters, and can take up to 64kB entry at once
	i := 1
	for scanner.Scan() {
		fmt.Println(i, ": ", scanner.Text())
		i++
	}
}

func main() {
	addFilenumberToFile()
}
