Elephants are the largest living animals on land. 
The three known species are the African bush elephant, the African Forest elephant and the Asian elephant. 
The African bush elephants are by far the largest, growing up to 3.36 m tall and weighing up to 6,914 kg. 

The African elephants are quite distinct in appearance from the Asian elephants. 
African elephants tend to be physically larger (weighing 4,000-7,500kg), with larger ears and concave backs.
Asian elephants tend to be smaller (weighing 3000-6,000kg), with smaller ears and convex backs.
African elephants also have full, rounded heads with a single dome at the top of their head. Asian elephants, meanwhile have twin-domed heads with a middle indentation.
