// Calculate maximal value in a slice
package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

func twoSum(nums []int, target int) []int {
	m := make(map[int]int)
	for idx, num := range nums {

		if v, found := m[target-num]; found {
			return []int{v, idx}
		}

		m[num] = idx
	}
	return nil
}

func ParseNumber() (nums []int, target int) {
	if len(os.Args) < 3 {
		log.Fatalf("Not enough arguments to run 2sum, example input: `go run . 14 1 2 5 8 12 15`")
	}
	nums = []int{}
	for _, newNumStr := range os.Args[2:] {
		newNum, err := strconv.Atoi(newNumStr)
		if err != nil {
			log.Fatalf("Could not convert to number: %s", newNumStr)
		}
		nums = append(nums, newNum)
	}
	target, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatalf("Could not convert target to number: %s", os.Args[1])
	}
	return nums, target
}

func main() {
	nums, target := ParseNumber()
	sol := twoSum(nums, target)
	fmt.Printf("Values which add up to %d: %d and %d", target, nums[sol[0]], nums[sol[1]])
}
