# Find Twosum

```bash
go run . 14 1 2 5 8 12 15
```

Zimt@LAPTOP-SHSPHKOE MINGW64 /c/code/gitlab.com/golangteam2022/gobasics/class_09_t_for_loops_challenge_twosum/00_train (develop)  
```bash
$ go run . 14 1 2 5 8 12 15
Values which add up to 14: 2 and 12
```