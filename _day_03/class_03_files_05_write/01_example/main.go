package main

import (
	"io/ioutil"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func CreateWith_os() {
	// os.Create calls the more generic OpenFile(name, O_RDWR|O_CREATE|O_TRUNC, 0666)
	f, err := os.Create("filename.ext") // creates a file at current directory
	check(err)
	defer f.Close()
}

func WriteWith_os() {
	// WriteFile writes data to the named file, creating it if necessary.
	text := []byte(`Those who cannot remember the past are condemned to repeat it (George Santayana)`)
	err := os.WriteFile("filename.ext", text, 0644) // creates a file at current directory
	check(err)
}

func WriteStringWith_ioutil() {
	s := []byte("This is a string") // convert string to byte slice
	// As of Go 1.16, this function simply calls os.WriteFile.
	ioutil.WriteFile("testfile.txt", s, 0644) // the 0644 is octal representation of the filemode
}

func WriteStringWith_os() {
	// more general opener
	// os.OpenFile("filename.ext", os.O_CREATE|os.O_WRONLY, 0644)

	// opener for existing files, most commonly used
	f, err := os.Open("filename.ext") // creates a file at current directory

	check(err)
	defer f.Close()

	//write directly into file
	f.Write([]byte("a string"))

	// write a string
	f.WriteString("\nThis is a pretty long string")

	// // write from a specific offset
	f.WriteAt([]byte("another string"), 42) // 12 is the offset from start
}

func main() {
	CreateWith_os()
	// WriteWith_os()
	// WriteStringWith_ioutil()
	// WriteStringWith_os()
}
