package main

import (
	"fmt"
	"os"
	//"os"
)

func main() {
	// example 1: failing
	badIndex()

	// example 2: causing panic by choice
	openWrongPath()

	// example 3: safe function which can recover from a panic
	v := safeValue([]int{1, 2, 3}, 10)
	fmt.Println(v)
}

func badIndex() {
	vals := []int{1, 2, 3}
	// This will cause a panic
	v := vals[10]
	fmt.Println(v)
}

func openWrongPath() {
	file, err := os.Open("no-such-file")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	fmt.Println("file opened")
}

func safeValue(vals []int, index int) int {
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("ERROR: %s\n", err)
		}
	}()

	return vals[index]
}
