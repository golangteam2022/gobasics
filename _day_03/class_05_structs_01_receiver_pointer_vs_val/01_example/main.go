package main

import "fmt"

type people []string

func (p people) add(name string) {
	p = append(p, name)
}

func (p *people) addPtr(name string) {
	*p = append(*p, name)
}

type human struct {
	name string
	age  int
}

func (h *human) setName(n string) {
	h.name = n
}
func (h *human) setAge(a int) {
	h.age = a
}

// why do we need this?
// because we do not have to recreate all type definition and method definition
type student = human

// This would not work, with this, it is not possible to call info(student{}), because info is defined for human
// this would only work, if we used methods, e.g. (h human) humanInfo()
// type student struct {
// 	human
// }

func (h human) humanInfo() {
	fmt.Printf("%T-Method: Hi my name is: %s and I'm: %d\n", h, h.name, h.age)
}

func info(h human) {
	fmt.Printf("Human-Function: Hi my name is: %s and I'm: %d\n", h.name, h.age)
}

func moreinfo(s student) {
	fmt.Printf("Student-Function: More info Hi my name is: %s and I'm: %d\n", s.name, s.age)
}

func modifyPeople() {
	world := &people{"John", "Steve"}
	world.add("Mike")    // didn't work
	fmt.Println(world)   // &[John Steve]
	world.addPtr("Mike") // worked
	fmt.Println(world)   // &[John Steve Mike]
}

func typeAliasing() {
	h := human{name: "John", age: 25}
	s := student{}
	s.setName("Jane")
	s.setAge(23)

	s.humanInfo() // Human-Method: Hi my name is: Jane and I'm: 23
	h.humanInfo()

	info(h) // Hi my name is: John and I'm: 25
	info(s) // Hi my name is: Jane and I'm: 23

	moreinfo(h)
	moreinfo(s)
}

func main() {
	modifyPeople()
	typeAliasing()
}
