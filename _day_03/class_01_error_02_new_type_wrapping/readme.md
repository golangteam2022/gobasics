# Error is an interface, not a string, and not a type!

```
go get github.com/pkg/errors
```

## Error is

Error is an Interface.
Error is NOT a type.

```go
type error interface {
	Error() string
}
```

Easy to create standard errors:
```go

import "errors"

// this is the issues
func Fail() error {
	myError := errors.New("Error here")
	return myError
}

```

Example from the hugo package
Easy to create standard errors:
```go

import "errors"

func NewPage(name string) (p *Page, err error) {
	if len(name) == 0 {
		// here is the issue:
		// this forces devs to than compare the string value
		return nil, errors.New("Zero length page name")
	}
	//...
}

```

Improvement:
```go
import "errors"

// provide exported Error, now devs can check value, and not the string of the error.
var ErrNoName = errors.New("Zero")

func NewPage(name string) (p *Page, err error) {
	if len(name) == 0 {
		// here is the issue:
		// this forces devs to than compare the string value
		return nil, ErrNoName
	}
	//...
}
```

> **Custom Errors** 
> - Guarantee Context, and consistent feedback
> - Provide type, which is  
> - safe for concurrent access

Internationalization of Errors.
Recent Pull Request for docker repository, decouple Error Code from error message.
Allow message in different languages.

And still adhere to Error interface by providing the Error() method providing a string.

```go
import (
	"fmt"
	"strconv"
	"strings"
)

type ErrorCode uint64

func (c ErrorCode) String() string {
	return strconv.Itoa(int(c))
}

type Error struct {
	Code    ErrorCode
	Message string
	Detail  interface{}
}

func (e Error) Error() string {
	return fmt.Sprintf("%s: %s",
		strings.ToLower(strings.Replace(e.Code.String(), "_", " ", -1)), e.Message)
}

```

The go Stdlib has really good examples.

### Example, Custom Errors in `os`

in `error.go` in package **os**

```go
import (
	"internal/oserror"
	"internal/poll"
	"io/fs"
)
var (
	// ErrInvalid indicates an invalid argument.
	// Methods on File will return this error when the receiver is nil.
	ErrInvalid = fs.ErrInvalid // "invalid argument"

	ErrPermission = fs.ErrPermission // "permission denied"
	ErrExist      = fs.ErrExist      // "file already exists"
	ErrNotExist   = fs.ErrNotExist   // "file does not exist"
	ErrClosed     = fs.ErrClosed     // "file already closed"

	ErrNoDeadline       = errNoDeadline()       // "file type does not support deadline"
	ErrDeadlineExceeded = errDeadlineExceeded() // "i/o timeout"
)
```

Or the defined PathError type in `io/fs` package, which automatically formats the error output using the operation, the path and the error:
```go
// PathError records an error and the operation and file path that caused it.
type PathError struct {
	Op   string
	Path string
	Err  error
}

func (e *PathError) Error() string { return e.Op + " " + e.Path + ": " + e.Err.Error() }

```
and its usage in **file.go**
```go
// ReadAt reads len(b) bytes from the File starting at byte offset off.
// It returns the number of bytes read and the error, if any.
// ReadAt always returns a non-nil error when n < len(b).
// At end of file, that error is io.EOF.
func (f *File) ReadAt(b []byte, off int64) (n int, err error) {
	if err := f.checkValid("read"); err != nil {
		return 0, err
	}

	if off < 0 {
		return 0, &PathError{Op: "readat", Path: f.name, Err: errors.New("negative offset")}
	}


```

Example 2:

Handle all Patherrors the same way.
Example function, showing how this could look like:
```go
func baa(f *file) error {
	n, err := f.WriteAt(x, 3)
	if _, ok := err.(*PathError) {
		//...
	} else {
		log.Fatalf(err)
	}
}
```
So we check if it possible to convert err into a type of pointer to a PathError, otherwise we will fail.

Seperating the error type from the error message gives great leverage for error handling.
