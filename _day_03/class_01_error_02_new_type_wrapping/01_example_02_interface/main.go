// pkg/errors example
package main

import (
	"errors"
	"fmt"
)

var DivisionByZero = errors.New("DivisionByZero")

type CustomError struct {
	ErrCode string
	Message string
	Err     error
}

func (e CustomError) Error() string { return e.ErrCode + ":" + e.Message + "-" + e.Err.Error() }

func CustomFail() error {
	err := &CustomError{"1001", "You are not allowed to divide by 0", DivisionByZero}
	return err
}

func main() {
	fmt.Println("==CustomFail==")
	err1 := CustomFail()
	fmt.Println(err1.Error())

	var customErr *CustomError
	if errors.As(err1, &customErr) { // true
		fmt.Println("err1 as &CustomError")
	}
	if errors.As(err1, customErr) { // false
		fmt.Println("err1 as CustomError")
	}
	if errors.Is(err1, customErr) { // true
		fmt.Println("err1 is CustomError")
	}
}
