# Log

The standard library package log provides a basic infrastructure for log management in GO language that can be used for logging our GO programs. The main purpose of logging is to get a trace of what's happening in the program, where it's happening, and when it's happening. Logs can be providing code tracing, profiling, and analytics. Logging(eyes and ears of a programmer) is a way to find those bugs and learn more about how the program is functioning.

The flags define the values which should be outputted with each output line

```go
log.SetFlags(log.Ldate | log.Lmicroseconds | log.Llongfile)
```

However the example will also use an `init` function, to initialize the package before any execution.