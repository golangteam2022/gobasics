package main

import (
	"fmt"
	"log"
)

func init() {
	/* TODO */
}

func fizzbuzz(num int) {
	/* TODO: replace the prints with logger calls */
	for i := 1; i <= num; i++ {
		if i%3 == 0 && i%5 == 0 {
			fmt.Println("fizz buzz")
		} else if i%3 == 0 {
			fmt.Println("fizz")
		} else if i%5 == 0 {
			fmt.Println("buzz")
		} else {
			fmt.Println(i)
		}
	}
}

func main() {
	var num int

	// Input a string
	fmt.Print("Input a string: ")
	n, err := fmt.Scanln(&num)
	if err != nil {
		log.Panicf(err.Error())
	}
	fmt.Println(n, err)
	fizzbuzz(num)
}
