package main

import (
	"fmt"
	"math"
)

type ErrNegativeSqrt float64

func (e ErrNegativeSqrt) Error() string {
	return fmt.Sprintf("cannot Sqrt negative number: %f", e)
}

func Sqrt(x float64) (float64, error) {
	if x < 0 {
		return 0, ErrNegativeSqrt(x)
	}

	// first approximation
	z := float64(1)
	tolerance := 1e-6

	for {
		// linear approximation step
		oldz := z
		// (z*z - x), how much off are we
		// (2 * z), derivative of z*z -> slope to get best step size for next approximation step
		z -= (z*z - x) / (2 * z)

		// abort the loop, if our approximation is close enough,
		// and the step we made is lower than the tolerance
		if math.Abs(z-oldz) < tolerance {
			break
		}
	}
	return z, nil
}
func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}
