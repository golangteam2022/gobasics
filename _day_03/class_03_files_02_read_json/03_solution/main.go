package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type Salary struct {
	Basic, HRA, TA float64
}

type Employee struct {
	FirstName, LastName, Email string
	Age                        int
	MonthlySalary              []Salary
}

func main() {
	jsonFilePath := "salaries.json"
	data, _ := os.ReadFile(jsonFilePath)

	var employee Employee
	json.Unmarshal(data, &employee)
	fmt.Println(employee)
}
