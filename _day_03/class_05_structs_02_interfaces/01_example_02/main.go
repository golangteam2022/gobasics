package main

type Vehicle interface {
	getCharge() int
	Drive()
}

func use(v Vehicle) {
	if v.getCharge() > 0 {
		v.Drive()
	}
}

type Car struct {
}

// using the pointer here would fail
func (c Car) Drive() {

}

func (c Car) getCharge() int {
	return 100
}

func main() {
	c := Car{}
	use(c)
}
