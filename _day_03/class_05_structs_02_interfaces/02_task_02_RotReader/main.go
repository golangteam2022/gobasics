package main

import (
	"io"
	"os"
	"strings"
)

const (
	LOWER = "abcdefghijklmnopqrstuvwxyz"
	UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	NUM   = len(LOWER)
)

type rotReader struct {
	/* implement the io.Reader interface, and enable adding a fixed shift of letters */
}

/* implement the methods necessary for the io.Reader interface*/

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rotReader{s, -13}
	io.Copy(os.Stdout, &r)
}
