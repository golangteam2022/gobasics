package main

import (
	"fmt"
	"io"
	"os"
)

// Capper implements io.Writer and turns everything to uppercase
type Upper struct {
	wtr io.Writer
}

func (c *Upper) Write(p []byte) (n int, err error) {
	for i, c := range p {
		// only modify lowercase characters
		if 97 <= c && c <= 122 {
			p[i] &= 0b11011111
		}
	}
	return c.wtr.Write(p)
}

func main() {
	// to print decimal and binary representation of a number:
	// fmt.Printf("Decimal value: %d\n", num)
	// fmt.Printf("Binary value : %b\n", num)

	c := &Upper{os.Stdout}
	text := "Hello World! Ready? ☺"

	fmt.Println("Original", text)
	fmt.Fprintln(c, "Upper: "+text)

}
