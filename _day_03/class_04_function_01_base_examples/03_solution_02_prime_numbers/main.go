package main

import "fmt"

func primeNumbers(n int) (primeNumbers []int) {
	var y int
	seen := make(map[int]bool)
	is_prime := make(map[int]bool)
	is_prime[2] = true
	is_prime[3] = true
	is_prime[5] = true
	seen[2] = true
	seen[3] = true
	seen[5] = true

	for i := 2; i < n; i++ {
		if !seen[i] && !is_prime[i] {
			is_prime[i] = true
		}
		if is_prime[i] {
			for y = i + i; y < n; y += i {
				is_prime[y] = false
				seen[y] = true
			}
		}
		seen[i] = true
	}

	for x := 0; x < n; x++ {
		if is_prime[x] {
			primeNumbers = append(primeNumbers, x)
		}
	}

	return
}

func main() {
	fmt.Println(primeNumbers(1000))
}
