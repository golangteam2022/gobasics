// pkg/errors example
package main

import (
	"fmt"
	"log"
	"os"
)

// Config holds configuration
type Config struct {
	// configuration fields go here (skipped)
}

func setupLogging() {
	out, err := os.OpenFile("app.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Cannot open File")
		return
	}
	log.SetOutput(out)
}

func main() {
	setupLogging()
}
